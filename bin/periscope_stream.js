/**
 * Created by vini on 12.01.16.
 */
var path = require('path');
var periscope = require('node-periscope-stream');

var path_steal_recorder = path.resolve(__dirname,'steal_recorder');
var steal_recorder = require(path_steal_recorder);

this.periscopeStream = function (id) {

    console.log('id', id);

    periscope('https://www.periscope.tv/w/'+id, function(err, details) {
        if (err) {
            console.log(err);
            return;
        }

        console.log('details', details.hls_url);
        if(details){
            steal_recorder.stealRecorder(details.hls_url);
        }
    });
};
