/**
 * Created by vini on 16.01.16.
 */

var fs = require('fs');
var spawn = require('child_process').spawn;
var path = require('path');

this.createName = function () {
    return 'video_' + Math.random().toString(36).substr(2, 9);
};

this.stealRecorder = function (rtspURI) {
    var ffmpeg = spawn('ffmpeg', ['-i', rtspURI, path.resolve(__dirname, '../', 'saved_video/'+this.createName()+'.mp4')]);

    ffmpeg.stdout.on('data', function (data) {
        console.log('stdout: ' + data);
    });

    ffmpeg.stderr.on('data', function (data) {
        console.log('stderr: ' + data);
    });

    ffmpeg.on('close', function (code) {
        console.log('child process exited with code ' + code);
    });
};
