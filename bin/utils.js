this.getCoUrls = function (data) {
    var coUrls = [];

    data.statuses.forEach(function (el) {
        var url = el.text.slice(el.text.indexOf('https'));
        var urlEnd = url.indexOf(' ');
        if(urlEnd > -1){ url = url.slice(0, urlEnd); }
        coUrls.push(url);
    });
    return coUrls;
};

this.getHrefIds = function (body) {
    var ids = [];
    if(!body){ return}
    var hrefIndex = body.indexOf('https://www.periscope.tv/');
    if(hrefIndex>-1) {
        //console.log('hrefIndex ', hrefIndex);
        var start = body.slice(hrefIndex);
        var endIndex = start.indexOf('"');
        console.log('start', start);

        var id = start.slice(endIndex - 13, endIndex)
        console.log('id', id);
        return id;
    }

    return ids;

};